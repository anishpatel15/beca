﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples
{
    public class SpeechToTextManager : MonoBehaviour
    {
        private GCSpeechRecognition _speechRecognition;

        private Button _startRecordButton,
                       _stopRecordButton;

        private Image _speechRecognitionState;

        private Text _speechRecognitionResult;

        private Toggle _isRuntimeDetectionToggle;

        private Dropdown _languageDropdown;

        private InputField _contextPhrases;

        public string spokenWords;

        private bool isRecording;

        [SerializeField]
        private int totalNumberOfSpokenWords;
        private float recordStartTime, totalRecordTime, totalPauseTime;

        [SerializeField]
        private float recordIntervalTime, pauseThreshold;

        private void Start()
        {
            _speechRecognition = GCSpeechRecognition.Instance;
            _speechRecognition.RecognitionSuccessEvent += RecognitionSuccessEventHandler;
            _speechRecognition.NetworkRequestFailedEvent += SpeechRecognizedFailedEventHandler;
            _speechRecognition.LongRecognitionSuccessEvent += LongRecognitionSuccessEventHandler;

            _startRecordButton = transform.Find("Canvas/Button_StartRecord").GetComponent<Button>();
            _stopRecordButton = transform.Find("Canvas/Button_StopRecord").GetComponent<Button>();

            _speechRecognitionState = transform.Find("Canvas/Image_RecordState").GetComponent<Image>();

            _speechRecognitionResult = transform.Find("Canvas/Text_Result").GetComponent<Text>();

            _isRuntimeDetectionToggle = transform.Find("Canvas/Toggle_IsRuntime").GetComponent<Toggle>();

            _languageDropdown = transform.Find("Canvas/Dropdown_Language").GetComponent<Dropdown>();

            _contextPhrases = transform.Find("Canvas/InputField_SpeechContext").GetComponent<InputField>();

            _startRecordButton.onClick.AddListener(StartRecordButtonOnClickHandler);
            _stopRecordButton.onClick.AddListener(StopRecordButtonOnClickHandler);

            _speechRecognitionState.color = Color.white;
            _startRecordButton.interactable = true;
            _stopRecordButton.interactable = false;

            _languageDropdown.ClearOptions();

            for (int i = 0; i < Enum.GetNames(typeof(Enumerators.LanguageCode)).Length; i++)
            {
                _languageDropdown.options.Add(new Dropdown.OptionData(((Enumerators.LanguageCode)i).ToString()));
            }

            _languageDropdown.onValueChanged.AddListener(LanguageDropdownOnValueChanged);

            _languageDropdown.value = _languageDropdown.options.IndexOf(_languageDropdown.options.Find(x => x.text == Enumerators.LanguageCode.en_GB.ToString()));

        }

        private void OnDestroy()
        {
            _speechRecognition.RecognitionSuccessEvent -= RecognitionSuccessEventHandler;
            _speechRecognition.NetworkRequestFailedEvent -= SpeechRecognizedFailedEventHandler;
            _speechRecognition.LongRecognitionSuccessEvent -= LongRecognitionSuccessEventHandler;
        }


        public void StartRecordButtonOnClickHandler()
        {
            _startRecordButton.interactable = false;
            _stopRecordButton.interactable = true;
            _speechRecognitionState.color = Color.red;
            _speechRecognitionResult.text = string.Empty;
            //_speechRecognition.StartRecord(_isRuntimeDetectionToggle.isOn);

            totalPauseTime = 0f;
            totalNumberOfSpokenWords = 0;

            isRecording = true;
            StartCoroutine(IntervalRecord(recordIntervalTime));
            recordStartTime = Time.time;
        }

        public void StopRecordButtonOnClickHandler()
        {
            Debug.Log("Stop Recording");

            ApplySpeechContextPhrases();

            _stopRecordButton.interactable = false;
            _speechRecognitionState.color = Color.yellow;

            isRecording = false;
            _speechRecognition.StopRecord();

            totalRecordTime = Time.time - recordStartTime;
            //CalculateWordsPerMinute();
        }

        IEnumerator IntervalRecord(float interval)
        {
            Debug.Log("Recording");

            _speechRecognition.StartRecord(_isRuntimeDetectionToggle.isOn);
            yield return new WaitForSeconds(interval);

            if (isRecording)
            {
                _speechRecognition.StopRecord();
                StartCoroutine(IntervalRecord(interval));
            }
        }

        private void CalculateSpeechInformation()
        {
            float wordsPerSecond, wordsPerMinute;
            //spokenWords += obj.results[0].alternatives[0].transcript;
            Debug.Log("Total Pause Time : " + totalPauseTime);
            Debug.Log("Total Number of Spoken Words :" + totalNumberOfSpokenWords);
            Debug.Log("Total Record Time :" + totalRecordTime);

            wordsPerSecond = totalNumberOfSpokenWords / totalRecordTime;
            Debug.Log("Words Per Second :" + wordsPerSecond);

            wordsPerMinute = wordsPerSecond * 60;

            Debug.Log("Words Per Minute :" + wordsPerMinute);

            TextAnalysisManager.instance.PaceCheck((int)wordsPerMinute);
            TextAnalysisManager.instance.FinalHesitationCheckScore(totalRecordTime, totalNumberOfSpokenWords);

            TextAnalysisManager.instance.gazeChecker.finishedSpeech = false;
        }

        private void LanguageDropdownOnValueChanged(int value)
        {
            _speechRecognition.SetLanguage((Enumerators.LanguageCode)value);
        }

        private void ApplySpeechContextPhrases()
        {
            string[] phrases = _contextPhrases.text.Trim().Split(","[0]);

            if (phrases.Length > 0)
                _speechRecognition.SetContext(new List<string[]>() { phrases });
        }

        private void SpeechRecognizedFailedEventHandler(string obj, long requestIndex)
        {
            _speechRecognitionResult.text = "Speech Recognition failed with error: " + obj;

            if (!_isRuntimeDetectionToggle.isOn)
            {
                _speechRecognitionState.color = Color.green;
                _startRecordButton.interactable = true;
                _stopRecordButton.interactable = false;
            }
        }

        private void RecognitionSuccessEventHandler(RecognitionResponse obj, long requestIndex)
        {
            if (!_isRuntimeDetectionToggle.isOn)
            {
                _startRecordButton.interactable = true;
                _speechRecognitionState.color = Color.green;
            }

            if (obj != null && obj.results.Length > 0)
            {
                _speechRecognitionResult.text = "Speech Recognition succeeded! Detected Most useful: " + obj.results[0].alternatives[0].transcript;    
            
                var words = obj.results[0].alternatives[0].words;
               
                float timeToSpeakWord;
                     
                for(int i =0 ; i < obj.results[0].alternatives[0].words.Length; i++)
                {
                    float wordStartTime, wordEndTime;

                    wordStartTime = float.Parse(obj.results[0].alternatives[0].words[i].endTime.Trim(new Char[] { 's' }), System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    wordEndTime = float.Parse(obj.results[0].alternatives[0].words[i].startTime.Trim(new Char[] {'s'}), System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    timeToSpeakWord = wordStartTime - wordEndTime;
                }

                TextAnalysisManager.instance.HesitationCheck(obj);

                //Debug.Log("Total Pause Time" + totalPauseTime);

                totalNumberOfSpokenWords += words.Length;

                //Debug.Log(totalNumberOfSpokenWords);

                if (words != null)
                {
                    string times = string.Empty;

                    foreach (var item in obj.results[0].alternatives[0].words)
                        times += "<color=green>" + item.word + "</color> -  start: " + item.startTime + "; end: " + item.endTime + "\n";

                    _speechRecognitionResult.text += "\n" + times;
                }

                string other = "\nDetected alternative: ";

                foreach (var result in obj.results)
                {
                    foreach (var alternative in result.alternatives)
                    {
                        if (obj.results[0].alternatives[0] != alternative)
                            other += alternative.transcript + ", ";
                    }
                }

                _speechRecognitionResult.text += other;
            }
            else
            {
                _speechRecognitionResult.text = "Speech Recognition succeeded! Words are no detected.";
            }

            if (!isRecording)
            {
                CalculateSpeechInformation();
            }
        }
     
        private void LongRecognitionSuccessEventHandler(OperationResponse operation, long index)
        {
            if (!_isRuntimeDetectionToggle.isOn)
            {
                _startRecordButton.interactable = true;
                _speechRecognitionState.color = Color.green;
            }

            if (operation != null && operation.response.results.Length > 0)
            {
                _speechRecognitionResult.text = "Long Speech Recognition succeeded! Detected Most useful: " + operation.response.results[0].alternatives[0].transcript;

                string other = "\nDetected alternative: ";

                foreach (var result in operation.response.results)
                {
                    foreach (var alternative in result.alternatives)
                    {
                        if (operation.response.results[0].alternatives[0] != alternative)
                            other += alternative.transcript + ", ";
                    }
                }

                _speechRecognitionResult.text += other;
                _speechRecognitionResult.text += "\nTime for the recognition: " + 
                    (operation.metadata.lastUpdateTime - operation.metadata.startTime).TotalSeconds + "s";
            }
            else
            {
                _speechRecognitionResult.text = "Speech Recognition succeeded! Words are no detected.";
            }
        }
    }
}