﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using FrostweepGames.Plugins.GoogleCloud.SpeechRecognition;
using System;
using System.Text.RegularExpressions;

public class TextAnalysisManager : MonoBehaviour
{
    public static TextAnalysisManager instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if(instance !=this)
        {
            Destroy(gameObject);
        }
    }

    [SerializeField]
    private int[] minExtremes, maxExtremes;

    [SerializeField]
    private TextMeshProUGUI finalScoreText, speechPaceScoreText, hesitationScoreText, gazeScoreText;

    [SerializeField]
    private TextMeshProUGUI speechPaceScoreFeedbackText, hesitationScoreFeedbackText, gazeScoreFeedbackText;

    [SerializeField]
    private string[] feedbackText;

    private int finalScore, speechPaceScore, hesitationScore, gazeScore;

    public GazeChecker gazeChecker;

    [SerializeField]
    private float pauseThreshold;

    private float totalPauseTime, timeToSpeakWord;

    public List<string> maybeFillerList, definiteFillerList;
    private int definiteFillerCount = 0, maybeFillerCount = 0;

    public List<float> fillerStartTime, fillerEndTime;

    private int fillerIndex;


    void Start()
    {
        /*
        string source = "Kind of I like it kind of";
        source = source.ToLower();

        Debug.Log(source);

        string search = "kind of";

        foreach (Match match in Regex.Matches(source, search))
        {
            Debug.Log(match.Index);
        }
        */


    }

    public void PaceCheck(int wordsPerMinute )
    {
        if ((wordsPerMinute > 0 && wordsPerMinute <= minExtremes[0]) || (wordsPerMinute > maxExtremes[0]))
        {
            speechPaceScoreText.text = "0/5";
            speechPaceScore = 0;
        }
        else if ((wordsPerMinute >= minExtremes[0] && wordsPerMinute < minExtremes[1]) || (wordsPerMinute <= maxExtremes[0] && wordsPerMinute > maxExtremes[1]))
        {
            speechPaceScoreText.text = "1/5";
            speechPaceScore = 1;
        }
        else if ((wordsPerMinute >= minExtremes[1] && wordsPerMinute < minExtremes[2]) || (wordsPerMinute <= maxExtremes[1] && wordsPerMinute > maxExtremes[2]))
        {
            speechPaceScoreText.text = "2/5";
            speechPaceScore = 2;
        }
        else if ((wordsPerMinute >= minExtremes[2] && wordsPerMinute < minExtremes[3]) || (wordsPerMinute <= maxExtremes[2] && wordsPerMinute > maxExtremes[3]))
        {
            speechPaceScoreText.text = "3/5";
            speechPaceScore = 3;
        }
        else if ((wordsPerMinute >= minExtremes[3] && wordsPerMinute < minExtremes[4]) || (wordsPerMinute <= maxExtremes[3] && wordsPerMinute > maxExtremes[4]))
        {
            speechPaceScoreText.text = "4/5";
            speechPaceScore = 4;
        }
        else if (wordsPerMinute >= minExtremes[4] && wordsPerMinute <= maxExtremes[4])
        {
            speechPaceScoreText.text = "5/5";
            speechPaceScore = 5;
        }

        speechPaceScoreFeedbackText.text = feedbackText[speechPaceScore];
    }

    public void HesitationCheck(RecognitionResponse obj)
    {
        //Debug.Log("Hesitation Check");

        for(int i =0 ; i < obj.results[0].alternatives[0].words.Length; i++)
        {
            float wordStartTime, wordEndTime;

            wordStartTime = float.Parse(obj.results[0].alternatives[0].words[i].endTime.Trim(new Char[] { 's' }), System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            wordEndTime = float.Parse(obj.results[0].alternatives[0].words[i].startTime.Trim(new Char[] {'s'}), System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            timeToSpeakWord = wordStartTime - wordEndTime;

            if (timeToSpeakWord > pauseThreshold)
            {
                totalPauseTime += (timeToSpeakWord - pauseThreshold);

                fillerStartTime.Add(wordStartTime);
                fillerEndTime.Add(wordEndTime);
            }  
        }
            
        DefiniteFillerCheck(obj.results[0].alternatives[0].transcript);
    }

    public void InfiniteFillerCheck(string transcript)
    {
        string formattedTranscript = transcript.ToLower();

        Debug.Log(formattedTranscript);

        int maybeFillerCount = 0;

        for (int i = 0; i < definiteFillerList.Count; i++)
        {
            string formattedFillerWord = definiteFillerList[i].ToLower();

            if (formattedTranscript.Contains(formattedFillerWord))
            {
                maybeFillerCount++;
            }
        }

        Debug.Log("Definite Filler Count " + maybeFillerCount);
    }

    public void DefiniteFillerCheck(string transcript)
    {
        string formattedTranscript = transcript.ToLower();
      
        for (int i = 0; i < definiteFillerList.Count; i++)
        {
            string formattedFillerWord = definiteFillerList[i].ToLower();

            if (formattedTranscript.Contains(formattedFillerWord))
            {
                definiteFillerCount++;
            }
        }

        Debug.Log("Definite Filler Count " + definiteFillerCount);
    }

    public void FinalHesitationCheckScore(float totalRecordTime, int totalNumberOfSpokenWords)
    {
        float pauseTimePercentage = (totalPauseTime / totalRecordTime) * 100f;

        Debug.Log("Pause Time Percentage" + pauseTimePercentage);

        Debug.Log("Definite filler count"+ definiteFillerCount + " " + totalNumberOfSpokenWords);

        float fillerPercentage = ((float)definiteFillerCount / (float)totalNumberOfSpokenWords) * 100f;

        Debug.Log("Filler Percentage" + fillerPercentage);

        hesitationScore = 5 - ((int)(pauseTimePercentage + fillerPercentage) / 40);
        hesitationScoreText.text = hesitationScore + "/5";

        hesitationScoreFeedbackText.text = feedbackText[hesitationScore];
    }

    public void GazeCheck()
    {
        float gazeCheckPercentage = (gazeChecker.gazeAtAudienceTime / gazeChecker.gazeCheckTotalTime) * 100f;

        Debug.Log("Gaze Percentage" + gazeCheckPercentage);

        if (gazeCheckPercentage >= 90f)
        {
            gazeScoreText.text = "5/5";
            gazeScore = 5;
        }
        else if (gazeCheckPercentage < 90 && gazeCheckPercentage >= 80)
        {
            gazeScoreText.text = "4/5";
            gazeScore = 4;
        }
        else if (gazeCheckPercentage < 80 && gazeCheckPercentage >= 70)
        {
            gazeScoreText.text = "3/5";
            gazeScore = 3;
        }
        else if (gazeCheckPercentage < 70 && gazeCheckPercentage >= 60)
        {
            gazeScoreText.text = "2/5";
            gazeScore = 2;
        }
        else if (gazeCheckPercentage < 60 && gazeCheckPercentage >= 50)
        {
            gazeScoreText.text = "1/5";
            gazeScore = 1;
        }
        else if (gazeCheckPercentage < 50)
        {
            gazeScoreText.text = "0/5";
            gazeScore = 0;
        }

        gazeScoreFeedbackText.text = feedbackText[gazeScore];

        CalculateFinalScore();
    }

    public void CalculateFinalScore()
    {
        finalScore = (int)(speechPaceScore + hesitationScore + gazeScore) / 3;
        finalScoreText.text = (int)finalScore+"/5";
    }
}
