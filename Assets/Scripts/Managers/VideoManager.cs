﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using Malee;

public class VideoManager : MonoBehaviour 
{
	[SerializeField, Reorderable(paginate = true, pageSize = 0)]
    public VideoListCollector videoListCollector;

	[System.Serializable]
    public struct MenuWindow 
    {
		public string menuWindowName;
		public Material skyboxMaterial;
			
       
//
        [SerializeField, Reorderable(paginate = true, pageSize = 0)]
        public VideoList videoClip;
    }

	[System.Serializable]
	public class VideoListCollector : ReorderableArray<MenuWindow> {
	}

    [System.Serializable]
	public class VideoList : ReorderableArray<VideoClip> {
    }
}
