﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class QAManager : MonoBehaviour
{
    public static QAManager instance = null;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    public GameObject[] correctAnswerGO;

    public int questionCounter;

    private bool secondTry;

    [SerializeField]
    private AudioAnimationScriptableObject[] qaAASO;

    [SerializeField]
    private TextMeshProUGUI[] subtitleTextBoxes;

    private Coroutine qaPlaySubtitlesCoroutine;

    public Button skipButton;
	
    public void CheckForCorrectAnswer()
    {
        if (VRUIManager.instance.selectedAnswerGO == correctAnswerGO[questionCounter])
        {
            Debug.Log("Correct");

            AssignGreenColor(VRUIManager.instance.selectedAnswerGO.GetComponent<Button>());

            VRUIManager.instance.selectedAnswerGO = null;

            if (questionCounter < 2)
            {
                //informationText[questionCounter].text = qaSO.subtitleInfoList[0].subtiteText;
                TriggerAudioAnimationScript(0);
                questionCounter++;
                secondTry = false;

                VRUIManager.instance.nextButton[questionCounter-1].SetActive(false);
                StartCoroutine(WaitAndGoToNextWindow(qaAASO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[0].audioClip.length));
            }
            else if(questionCounter == 2) //Last question
            {
                //informationText[questionCounter].text = qaSO.subtitleInfoList[1].subtiteText;
                TriggerAudioAnimationScript(1);

                VRUIManager.instance.nextButton[questionCounter].SetActive(false);
                StartCoroutine(WaitAndGoToNextWindow(qaAASO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[1].audioClip.length));
            }
                
           
        }
        else
        {
            Debug.Log("Wrong");

            VRUIManager.instance.selectedAnswerGO.GetComponent<Button>().interactable = false;

            Color wrongColor;
            ColorUtility.TryParseHtmlString("#D25050", out wrongColor);

            ColorBlock tempColorBlock = VRUIManager.instance.selectedAnswerGO.GetComponent<Button>().colors;
            tempColorBlock.disabledColor = wrongColor;
            VRUIManager.instance.selectedAnswerGO.GetComponent<Button>().colors = tempColorBlock;

            if (questionCounter <= 2 && !secondTry)
            {
                //Debug.Log(questionCounter);
                //informationText[questionCounter].text = qaSO.subtitleInfoList[2].subtiteText;
                TriggerAudioAnimationScript(2);
                secondTry = true;
            }
            else if (questionCounter < 2 && secondTry)
            {
                //informationText[questionCounter].text = qaSO.subtitleInfoList[3].subtiteText;
                TriggerAudioAnimationScript(3);
                secondTry = false;

                //Debug.Log("Highlight Correct" + questionCounter);
                AssignGreenColor(correctAnswerGO[questionCounter].GetComponent<Button>());
                questionCounter++;

                VRUIManager.instance.nextButton[questionCounter-1].SetActive(false);
                StartCoroutine(WaitAndGoToNextWindow(qaAASO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[3].audioClip.length));
            }
            else if(questionCounter == 2 && secondTry)
            {
                //informationText[questionCounter].text = qaSO.subtitleInfoList[4].subtiteText;
                TriggerAudioAnimationScript(4);
          
                AssignGreenColor(correctAnswerGO[questionCounter].GetComponent<Button>());

                VRUIManager.instance.nextButton[questionCounter].SetActive(false);
                StartCoroutine(WaitAndGoToNextWindow(qaAASO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[4].audioClip.length));   
            }

            VRUIManager.instance.selectedAnswerGO = null;
        }
    }

    //Color set only when the button in not interactable
    public void AssignGreenColor(Button button)
    {
        button.interactable = false;

        Color correctColor;
        ColorUtility.TryParseHtmlString("#25C631", out correctColor);

        ColorBlock tempColorBlock = button.colors;
        tempColorBlock.disabledColor = correctColor;
        button.colors = tempColorBlock;
    }

    IEnumerator WaitAndGoToNextWindow(float waitTime)
    {
        skipButton.interactable = false;
        //Replace this with Animation - Audio Clip Length
        yield return new WaitForSeconds(waitTime);

        skipButton.interactable = true;

        VideoController.instance.NextWindow();
    }

    public void TriggerAudioAnimationScript(int index)
    {
        if (qaAASO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].audioClip !=null)
        {
            //Debug.Log("Playing Index" + index);

            if (VideoController.instance.becaAudioSource.isPlaying)
            {
                VideoController.instance.becaAudioSource.Stop();
            }

            VideoController.instance.becaAudioSource.clip = qaAASO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].audioClip;
            VideoController.instance.becaAudioSource.Play();

            string animationName = qaAASO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].animationName;

            if (animationName == "isTalking")
            {
                VideoController.instance.becaAnimator.SetBool("isTalking", true);
            }
            //HACK: Should be refactored to the correct animations with delays
            else if (animationName == "isCorrect")
            {
                VideoController.instance.becaAnimator.SetBool("isTalking", true);
            }
            else if (animationName == "isWrong")
            {
                VideoController.instance.becaAnimator.SetBool("isTalking", true);
            }
        }

        if (qaPlaySubtitlesCoroutine != null)
        {
            StopCoroutine(qaPlaySubtitlesCoroutine);
        }

        //Disable subtitle play from VideoController script to prevent conflict
        if (VideoController.instance.playSubtitlesCoroutine != null)
        {
            VideoController.instance.StopPlaySubtitlesCoroutine();
        }

        qaPlaySubtitlesCoroutine = StartCoroutine(PlaySubtitles(index, questionCounter));
    }

    IEnumerator PlaySubtitles(int index, int textBoxIndex)
    {
        //Finding Total time to run the coroutine for
        float totalSubtitleTime = 0;

        for (int i = 0; i < qaAASO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].subtitleInfoList.Length; i++)
        {
            totalSubtitleTime += qaAASO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].subtitleInfoList[i].subtitleDisplayTime;
        }

        float startTime = Time.time;
        float deltaTime = 0;

        float displayTime = qaAASO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].subtitleInfoList[0].subtitleDisplayTime;
        subtitleTextBoxes[textBoxIndex].text = qaAASO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].subtitleInfoList[0].subtiteText;

        int j = 0;

        //subtracting 1 to prevent the coroutine from jumping just over the totalSubtitleTime
        while (deltaTime < (totalSubtitleTime-1))
        {
            deltaTime = Time.time - startTime;
            //Debug.Log(deltaTime + " " + displayTime);
            //Debug.Log(qaAASO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].subtitleInfoList[j].subtiteText);
            if (deltaTime > displayTime)
            {
                j++;
                displayTime+= qaAASO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].subtitleInfoList[j].subtitleDisplayTime;
                subtitleTextBoxes[textBoxIndex].text = qaAASO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].subtitleInfoList[j].subtiteText;
            }

            yield return null;
        }

        VideoController.instance.becaAnimator.SetBool("isTalking", false);
    }

    public void PublicSkipQuestion()
    {
        if (VideoController.instance.currentWindowIndex == 3 || VideoController.instance.currentWindowIndex == 4)
        {
            questionCounter++;

            if (VideoController.instance.playSubtitlesCoroutine != null)
            {
                VideoController.instance.StopPlaySubtitlesCoroutine();
                VideoController.instance.becaAudioSource.Stop();
                VideoController.instance.becaAnimator.SetBool("isTalking", false);
            }
        }

        if (VideoController.instance.currentWindowIndex == 5)
        {
            skipButton.interactable = false;
        }
    }
        
}
