﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager instance = null;

//	[SerializeField]
//	private Text txt;

//	private AnalyticsTracker tracker;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if(instance !=this)
        {
            Destroy(gameObject);
        }
    }
       
    public MenuWindowInfo menuWindowInfo;
    public int currentWindowIndex;

    [SerializeField]
    private GameObject subtitleWindow;

    public AudioSource audioSource;

   // public GameObject[] answerGameObjectList;

	private string scene;


    [Header("Audio/Animation/Subtitle")]
    [Space]

    [SerializeField]
    private AudioAnimationScriptableObject[] aaSO;

    [SerializeField]
    private TextMeshProUGUI[] subtitleTextBoxes;

    [SerializeField]
    private GameObject becaGO;
   
    [SerializeField]
    private Animator becaAnimator;

    [SerializeField]
    private AudioSource becaAudioSource;

    public Coroutine playSubtitlesCoroutine;

    public GameObject redBanner, whiteBanner;

    public GameObject exp1RedBanner, exp1WhiteBanner;

    public bool userHasAlreadyUsedTheApp;


    //Cam 0 = AR canera ; cam 1 = static camera; cam 2 = Normal Camera
	[SerializeField]
	private GameObject[] cameras;

    void Start()
    {
        if(PlayerPrefs.GetInt ("NewInstall", 0) == 1)
        {
            userHasAlreadyUsedTheApp = true;
        }

        if (userHasAlreadyUsedTheApp)
        {
            currentWindowIndex = 6;
            SetActiveCamera(0);
            exp1RedBanner.SetActive(true);
            exp1WhiteBanner.SetActive(false);

            NextWindow();
            menuWindowInfo.menuWindowList[0].menuWindowGO.SetActive(false);
        }
        else
        {
            SetActiveCamera (2);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown("a"))
        {
           NextWindow();
            //AssignTextToWindowTextBoxes();
        }

    }

    public void NextWindow()
    {
        menuWindowInfo.menuWindowList[currentWindowIndex].menuWindowGO.SetActive(false);

        currentWindowIndex++;
        menuWindowInfo.menuWindowList[currentWindowIndex].menuWindowGO.SetActive(true);
        AssignTextToWindowTextBoxes();
        MainMenuSpeakingTrigger(currentWindowIndex);
    }

    public void AssignTextToWindowTextBoxes()
    {
        int languageIndex = (int) SceneInfoManager.instance.selectedLanguage;

        //Debug.Log(languageIndex);
        for (int i = 0; i < menuWindowInfo.menuWindowList[currentWindowIndex].textGOList.Count; i++)
        {
            
            menuWindowInfo.menuWindowList[currentWindowIndex].textGOList[i].GetComponent<TextMeshProUGUI>().text = 
                SceneInfoManager.instance.languageSOList[languageIndex].menuWindowList[currentWindowIndex].textInfoList[i].textInfo;
            menuWindowInfo.menuWindowList[currentWindowIndex].textGOList[i].GetComponent<TextMeshProUGUI>().parseCtrlCharacters = false;
            menuWindowInfo.menuWindowList[currentWindowIndex].textGOList[i].GetComponent<TextMeshProUGUI>().parseCtrlCharacters = true;
        }
    }

    private void MainMenuSpeakingTrigger(int currentWindowIndex)
    {
        switch (currentWindowIndex)
        {
            case 0:
				
                break;

            case 1:
				
				SetActiveCamera (0);
                break;

            case 2:
                TriggerAudioAnimationScript(1);
                break;

            case 3:
                TriggerAudioAnimationScript(2);
                break;

            case 4:
                TriggerAudioAnimationScript(3);
                break;

            case 5:

                TriggerAudioAnimationScript(4);
                break;

            case 6:

                TriggerAudioAnimationScript(5);
                break;

            case 7:
				SetActiveCamera (0);
                if (!userHasAlreadyUsedTheApp)
                {
                    TriggerAudioAnimationScript(6);
                }
                break;

            case 8:
				SetActiveCamera (1);
                TriggerAudioAnimationScript(7);
                break;
        }
    }

    public void InvokeNextWindow()
    {
        Debug.Log("Invoked next window");
        Invoke("NextWindow", 40f);
    }

    public void TriggerAudioAnimationScript(int index)
    {
        if (aaSO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].audioClip != null)
        {
            if (becaAudioSource.isPlaying)
            {
                becaAudioSource.Stop();
            }

            becaAudioSource.clip = aaSO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].audioClip;
            becaAudioSource.Play();

            if (aaSO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].animationName == "isTalking")
            {
                becaAnimator.SetBool("isTalking", true);
            }
        }

        if (playSubtitlesCoroutine != null)
        {
            StopCoroutine(playSubtitlesCoroutine);
        }

        playSubtitlesCoroutine = StartCoroutine(PlaySubtitles(index));
    }

    public void StopPlaySubtitlesCoroutine()
    {
        StopCoroutine(playSubtitlesCoroutine);
    }

    IEnumerator PlaySubtitles(int index)
    {
        Debug.Log(index);
        //Finding Total time to run the coroutine for
        float totalSubtitleTime = 0;

        for (int i = 0; i < aaSO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].subtitleInfoList.Length; i++)
        {
            totalSubtitleTime += aaSO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].subtitleInfoList[i].subtitleDisplayTime;
        }

        float startTime = Time.time;
        float deltaTime = 0;

        float displayTime = aaSO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].subtitleInfoList[0].subtitleDisplayTime;
        subtitleTextBoxes[aaSO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].subtitleTextboxIndex].text 
        = aaSO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].subtitleInfoList[0].subtiteText;

        int j = 0;

        //subtracting 1 to prevent the coroutine from jumping just over the totalSubtitleTime
        while (deltaTime < (totalSubtitleTime-1))
        {
            deltaTime = Time.time - startTime;

            if (deltaTime > displayTime)
            {
                j++;
                displayTime+= aaSO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].subtitleInfoList[j].subtitleDisplayTime;
                subtitleTextBoxes[aaSO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].subtitleTextboxIndex].text
                = aaSO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].subtitleInfoList[j].subtiteText;
            }

            yield return null;
        }

        becaAnimator.SetBool("isTalking", false);
    }

    public void SelectLanguage(int selectedLanguageIndex)
    {
        SceneInfoManager.instance.selectedLanguage = (SceneInfoManager.Languages) selectedLanguageIndex ;
        subtitleWindow.SetActive(false);

        AssignTextToWindowTextBoxes();
    }


	public void DebugClick(){
		Debug.Log ("Click me ");
		//SceneManager.LoadScene("Public_Scene");
	}

	public void MenuExperience(string str){
		scene = str;
		NextWindow ();
	}


	public void ChangeScene (bool isVR){
		if (isVR) {
			PlayerPrefs.SetInt ("isVrEnabled", 1);
		} else {
			
			PlayerPrefs.SetInt ("isVrEnabled", 0);
		}

		PlayerPrefs.Save ();
		if (scene != null) {
			SceneManager.LoadScene (scene);
			scene = "";
		}
	}

	public int GetScreenPos(){
		return currentWindowIndex;
	}
//	public void CallAnalytics(){
//		//gameObject.GetComponent<AnalyticsTracker> ().SendToAnalytics ();
//		tracker.SendToAnalytics ();
//	}

	public void SetActiveCamera(int pos)
	{
		for (int i = 0; i < cameras.Length; i++) 
		{
			if (i == pos) 
			{
				cameras [i].SetActive (true);
			} else 
			{
				cameras [i].SetActive (false);
			}
		}
	}

}
