﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VRUIManager : MonoBehaviour 
{
    public static VRUIManager instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    public GameObject selectedAnswerGO;

    [SerializeField]
    private Color normalColor = new Color();

    public GameObject[] nextButton;

    public void SelectAnswer(GameObject selectedBtnGO)
    {
        nextButton[QAManager.instance.questionCounter].SetActive(true);

        Button selectedAnswerBtn;

        //Previously selected Button reset
        if (selectedAnswerGO != null)
        {   
            selectedAnswerBtn = selectedAnswerGO.GetComponent<Button>();

            ColorBlock tempColorBlock1 = selectedAnswerBtn.colors;
            tempColorBlock1.normalColor = normalColor;
            selectedAnswerBtn.colors = tempColorBlock1;

            selectedAnswerBtn.interactable = true;
        }

        selectedAnswerGO = selectedBtnGO;

        selectedAnswerBtn = selectedAnswerGO.GetComponent<Button>();

        ColorBlock tempColorBlock2 = selectedAnswerBtn.colors;
        tempColorBlock2.normalColor = selectedAnswerBtn.colors.highlightedColor;
        selectedAnswerBtn.colors = tempColorBlock2;

        selectedAnswerBtn.interactable = false;
    }


}
