﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Malee;

public class SceneInfoManager : MonoBehaviour 
{
    public static SceneInfoManager instance = null;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if(instance !=this)
        {
            Destroy(gameObject);
        }
    }


    public enum Languages
    {
        English, Vietnamese
    }

    public Languages selectedLanguage;

    public LanguageScriptableObject[] languageSOList;

}


