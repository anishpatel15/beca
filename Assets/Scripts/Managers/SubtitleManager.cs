﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SubtitleManager : MonoBehaviour 
{
    public static SubtitleManager instance = null;

    float totalSubtitleTime;
	private TextMeshProUGUI subtitleText; 

	[SerializeField]
	private TextMeshProUGUI[] textBox;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if(instance !=this)
        {
            Destroy(gameObject);
        }


    }
        
    public SubtitleScriptableObject[] subtitleScriptableObject;

	public void StartSubtitles(int currentIndex)
    {
		Debug.Log ("Anish index = " + currentIndex);
		Debug.Log ("Anish legnth of infolist = " + subtitleScriptableObject [0].windowInfoList.Length);
		if (currentIndex < subtitleScriptableObject [0].windowInfoList.Length) {
			if (textBox [subtitleScriptableObject [0].windowInfoList [currentIndex].textBoxPos] != null) {
				subtitleText = textBox [subtitleScriptableObject [0].windowInfoList [currentIndex].textBoxPos];
				if (subtitleText != null) {
					TotalSubtitleTime (currentIndex);
					StartCoroutine (PlaySubtitles (currentIndex));
				}
			}
		}
    }

	void TotalSubtitleTime(int index)
    {
		for (int i = 0; i < subtitleScriptableObject [0].windowInfoList [index].subtitleInfoList.Length; i++) {
			totalSubtitleTime += subtitleScriptableObject [0].windowInfoList [index].subtitleInfoList [i].subtitleDisplayTime;

		}
    }

	IEnumerator PlaySubtitles(int index)
    {
        float startTime = Time.time;
        float deltaTime = 0;

		float displayTime = subtitleScriptableObject[0]. windowInfoList [index].subtitleInfoList[0].subtitleDisplayTime;

//		subtitleScriptableObject[(int)SceneInfoManager.instance.selectedLanguage]. windowInfoList [index].textBox.text 
		subtitleText.text = subtitleScriptableObject[0].windowInfoList [index].subtitleInfoList[0].subtiteText;

        int i = 0;

        //subtracting 1 to prevent the coroutine from jumping just over the totalSubtitleTime
        while (deltaTime < (totalSubtitleTime-1))
        {
            deltaTime = Time.time - startTime;

            if (deltaTime > displayTime)
            {
                i++;
				displayTime+= subtitleScriptableObject[0].windowInfoList [index].subtitleInfoList[i].subtitleDisplayTime;
//				subtitleScriptableObject[(int)SceneInfoManager.instance.selectedLanguage]. windowInfoList [index].textBox.text 
				subtitleText.text = subtitleScriptableObject[0].windowInfoList [index].subtitleInfoList[i].subtiteText;
            }

            yield return null;
        }
    }

}
