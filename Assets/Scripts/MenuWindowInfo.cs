﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Malee;

//Holds the TextMeshPro GameObjects for each Window
public class MenuWindowInfo : MonoBehaviour 
{
	[SerializeField, Reorderable(paginate = true, pageSize = 0)]
    public MenuWindowList menuWindowList;

	[System.Serializable]
    public struct MenuWindow 
    {
		public string menuWindowName;
		public GameObject menuWindowGO;
	
        [SerializeField, Reorderable(paginate = true, pageSize = 0)]
        public TextGOList textGOList;
    }

	[System.Serializable]
    public class MenuWindowList : ReorderableArray<MenuWindow> {
	}

    [System.Serializable]
    public class TextGOList : ReorderableArray<GameObject> {
    }
}
