﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class GyroController : MonoBehaviour 
{
    Vector3 FirstPoint;
    Vector3 SecondPoint;
    float xAngle, xAngleTemp;
    float yAngle, yAngleTemp;
    string debugText = "";

    //If Phone has GyroScope 
    private bool gyroEnabled;
    private Gyroscope gyro;

    private Quaternion adjustrot;
    private float fNewInputX, fNewInputY;
    private float fPreviousInputX, fPreviousInputY;

    private bool inVRMode = true;

    [SerializeField]
    private float fDamping = 80f;

	void Start () 
    {
        EnableGyro();
        StartCoroutine(SwitchToVR());

        if (PlayerPrefs.GetInt("isVrEnabled") == '0')
        {
            ChangeViewType();
        }
     
        //XRSettings.LoadDeviceByName("cardboard");
        //XRSettings.enabled = true;   
	}

    IEnumerator SwitchToVR() {
        // Device names are lowercase, as returned by `XRSettings.supportedDevices`.
        string desiredDevice = "cardboard"; 

        if (string.Compare(XRSettings.loadedDeviceName, desiredDevice, true) != 0) 
        {
            XRSettings.LoadDeviceByName(desiredDevice);
            // Must wait one frame after calling `XRSettings.LoadDeviceByName()`.
            yield return null;
        }

        // Now it's ok to enable VR mode.
        XRSettings.enabled = true;
        //XRSettings.enabled = true;
    }



    private void EnableGyro()
    {
        if (SystemInfo.supportsGyroscope)
        {
            gyro = Input.gyro;
            gyro.enabled = true;
        }
    }
	
    void Update () {


        #if UNITY_ANDROID || UNITY_IOS
        if(inVRMode == false)
        {
            if (gyroEnabled) 
            {
                //Allows for Finger and Camera rotation 
                this.transform.localRotation = //CalcRotation() * 
                    InputTracking.GetLocalRotation(XRNode.Head);

            } else{
                //Allows Finger rotation NO Gyroscope
                //this.transform.localRotation = CalcRotation();
            }

        }
        else
        {
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                //inVRMode = false;
                //StartCoroutine (SwitchTo2D ());
            }
        }


        fPreviousInputX = fNewInputX;
        fPreviousInputY = fNewInputY;


        #else
        //Unity edittor for Debugging. 
        if(Input.GetKey(KeyCode.UpArrow))
        transform.Rotate(Vector3.left, turnSpeed * Time.deltaTime);

        if(Input.GetKey(KeyCode.DownArrow))
        transform.Rotate(Vector3.right, turnSpeed * Time.deltaTime);

        if(Input.GetKey(KeyCode.LeftArrow))
        transform.Rotate(Vector3.up, -turnSpeed * Time.deltaTime);

        if(Input.GetKey(KeyCode.RightArrow))
        transform.Rotate(Vector3.up, turnSpeed * Time.deltaTime);
        #endif
    }


    private Quaternion CalcRotation(){

        if(Input.touchCount > 0)
        {
            //if fingeres are used
            if(Input.GetTouch(0).phase == TouchPhase.Began)
            {
                FirstPoint = Input.GetTouch(0).position;
                xAngleTemp = xAngle;
                yAngleTemp = yAngle;
            }

            if(Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                SecondPoint = Input.GetTouch(0).position;

                xAngle = xAngleTemp + (SecondPoint.x - FirstPoint.x) * 180 / Screen.width;
                yAngle = yAngleTemp + (SecondPoint.y - FirstPoint.y) * 90 / Screen.height;

                //fNewInputX = xAngle;
                fNewInputX = Mathf.Lerp( fPreviousInputX, xAngle, fDamping*Time.deltaTime );
                fNewInputY = Mathf.Lerp( fPreviousInputY, yAngle, fDamping*Time.deltaTime );
                adjustrot = Quaternion.Inverse(Quaternion.Euler((fNewInputY), fNewInputX, 0.0f));

            }
        }
        return adjustrot;
    }

    public void ChangeViewType()
    {
        XRSettings.enabled = !XRSettings.enabled;
        inVRMode = !inVRMode;
        gyroEnabled = !gyroEnabled;

        ResetCameras();
    }

    void ResetCameras()
    {
        // Camera looping logic copied from GvrEditorEmulator.cs
        for (int i = 0; i < Camera.allCameras.Length; i++)
        {
            Camera cam = Camera.allCameras[i];
            if (cam.enabled && cam.stereoTargetEye != StereoTargetEyeMask.None)
            {

                // Reset local position.
                // Only required if you change the camera's local position while in 2D mode.
                cam.transform.localPosition = Vector3.zero;

                // Reset local rotation.
                // Only required if you change the camera's local rotation while in 2D mode.
                cam.transform.localRotation = Quaternion.identity;

            }
        }
    }
}
