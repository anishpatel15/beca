﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.UI;
using TMPro;


public class AnalyticsTracker : MonoBehaviour {

	[SerializeField]
	private GameObject[] answerGameObjectList;

	[SerializeField]
	private string key;


	public void SendToAnalytics()
	{
		 
		int count = 0;
		Dictionary<string, object> parameters = new Dictionary<string, object>();

		foreach (GameObject answerObject in answerGameObjectList) 
		{
			Toggle toggle = answerObject.GetComponent<Toggle> ();

			if (toggle.isOn) 
			{
				Debug.Log(toggle.GetComponentInChildren<TextMeshProUGUI> ().text);
				parameters.Add ("value-" + count, toggle.GetComponentInChildren<TextMeshProUGUI> ().text);
				count += 1;
			}
		}

		// Send event
		AnalyticsResult result = AnalyticsEvent.Custom(key, parameters);
		if(result == AnalyticsResult.Ok)
		{
			Debug.Log (" Analytics Result Success");
		} else 
		{
			Debug.Log (" Analytics Result Fail");
		}
	}
}
