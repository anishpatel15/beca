﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Malee;

[CreateAssetMenu(menuName = "ScriptableObject/AudioAnimationSO", fileName = "AudioAnimationSO1", order = 1)]
public class AudioAnimationScriptableObject : ScriptableObject 
{
    public enum Languages
    {
        English, Vietnamese
    }

    public Languages language;

    [SerializeField, Reorderable(paginate = true, pageSize = 0, elementNameProperty = "myString")]
    public AudioAnimationInfoList audioAnimationInfoList;

    [System.Serializable]
    public struct AudioAnimationInfo 
    {
        public string description;
        public AudioClip audioClip;
        public string animationName;  

        public int subtitleTextboxIndex;

        [SerializeField, Reorderable(paginate = true, pageSize = 0, elementNameProperty = "myString")]
        public SubtitleInfoList subtitleInfoList;
    }

    [System.Serializable]
    public struct SubtitleInfo 
    {
        public string subtiteText;   
        public float subtitleDisplayTime;

    }

    [System.Serializable]
    public class SubtitleInfoList : ReorderableArray<SubtitleInfo> {
    }

    [System.Serializable]
    public class AudioAnimationInfoList : ReorderableArray<AudioAnimationInfo> {
    }
}
