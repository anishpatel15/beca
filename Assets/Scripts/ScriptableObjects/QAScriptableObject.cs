﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Malee;

[CreateAssetMenu(menuName = "ScriptableObject/QA", fileName = "QAData", order = 1)]
public class QAScriptableObject : ScriptableObject
{

    public LanguageScriptableObject.Languages language;

    [SerializeField, Reorderable(paginate = true, pageSize = 0, elementNameProperty = "myString")]
    public SubtitleInfoList subtitleInfoList;

    [System.Serializable]
    public struct resultInfo 
    {
        public string resultName;
        public string subtiteText;   
        public float subtitleDisplayTime;
    }

    [System.Serializable]
    public class SubtitleInfoList : ReorderableArray<resultInfo> {
    }
}
