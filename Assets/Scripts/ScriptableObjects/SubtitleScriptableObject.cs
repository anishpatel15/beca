﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Malee;
using TMPro;

[CreateAssetMenu(menuName = "ScriptableObject/Subtitle", fileName = "SubtitleData", order = 1)]
public class SubtitleScriptableObject : ScriptableObject
{

    public LanguageScriptableObject.Languages language;

   

	[SerializeField, Reorderable(paginate = true, pageSize = 0, elementNameProperty = "myString")]
	public WindowList windowInfoList;

    [System.Serializable]
    public struct SubtitleInfo 
    {
        public string subtiteText;   
        public float subtitleDisplayTime;
    }

    [System.Serializable]
    public class SubtitleInfoList : ReorderableArray<SubtitleInfo> {
    }

	[System.Serializable]
	public struct Window 
	{
		
		public string windowText;  


		public int textBoxPos;

		[SerializeField, Reorderable(paginate = true, pageSize = 0, elementNameProperty = "myString")]
		public SubtitleInfoList subtitleInfoList;

	}

	[System.Serializable]
	public class WindowList : ReorderableArray<Window> {
	}


}
