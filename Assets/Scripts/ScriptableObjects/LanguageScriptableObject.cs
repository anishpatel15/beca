﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Malee;

[CreateAssetMenu(menuName = "ScriptableObject/Language", fileName = "LanguageScriptableObject", order = 1)]
public class LanguageScriptableObject : ScriptableObject 
{
    public enum Languages
    {
        English, Vietnamese
    }

    public Languages language;
	[SerializeField, Reorderable(paginate = true, pageSize = 0, elementNameProperty = "myString")]
    public MenuWindowList menuWindowList;

	[System.Serializable]
    public struct MenuWindow 
    {
        public string windowName;	

        [SerializeField, Reorderable(paginate = true, pageSize = 0, elementNameProperty = "myString")]
        public TextInfoList textInfoList;
	}

    [System.Serializable]
    public struct TextInfo 
    {
        public string textInfo;   
    }

	[System.Serializable]
    public class MenuWindowList : ReorderableArray<MenuWindow> {
	}

    [System.Serializable]
    public class TextInfoList : ReorderableArray<TextInfo> {
    }
}
