﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using TMPro;
using UnityEngine.SceneManagement;
using Malee;


public class test_ : MonoBehaviour {


	//This section is used to controll which videos play at which time. 
	//Make sure this list has the same number as the MenuWindowInfo
	[SerializeField, Reorderable(paginate = true, pageSize = 0)]
	public VideoListCollector videoListCollector;

	[System.Serializable]
	public struct VideoWindow 
	{
		[SerializeField]
		public string menuWindowName;
		[SerializeField]
		public Material skyboxMaterial;
		[SerializeField]
		public bool isStaticSkybox;

		[SerializeField, Reorderable(paginate = true, pageSize = 0)]
		public VideoList videoClip;
	}

	[System.Serializable]
	public class VideoListCollector : ReorderableArray<VideoWindow> {}

	[System.Serializable]
	public class VideoList : ReorderableArray<VideoClip> {}

	[SerializeField]
	private int currentWindowIndex;

	[SerializeField]
	private RenderTexture _renderTexture = null;

	[SerializeField]
	private TextMeshProUGUI txt;

	[SerializeField]
	private MenuWindowInfo menuWindowInfo;



	private VideoPlayer videoPlayer;
	private AudioSource audioSource;
	private int nextVideo = 0;



	void Start()
	{
		//NextWindow();
		RenderSettings.skybox = videoListCollector [currentWindowIndex].skyboxMaterial;
	}

	void Update()
	{
		if (Input.GetKeyDown("a")){
			NextWindow();
		}

	}

	//Play next in timeline
	public void NextWindow()
	{
		//Stop video play when moving to next screen 
		StopVideo ();
		menuWindowInfo.menuWindowList[currentWindowIndex].menuWindowGO.SetActive(false);

		//Move to next item
		currentWindowIndex++;

		//Reset next video list back to 0
		nextVideo = 0;

		//Change the Skybox material from either video or static 
		RenderSettings.skybox = videoListCollector [currentWindowIndex].skyboxMaterial;

		//Start Video playing 
		if (!videoListCollector [currentWindowIndex].isStaticSkybox) {

			StartCoroutine (SetUpVideo(currentWindowIndex));
		}

		//Make next Window visable
		menuWindowInfo.menuWindowList[currentWindowIndex].menuWindowGO.SetActive(true);

	}



	private IEnumerator SetUpVideo(int index)
	{
		//		Debug.Log ("videolistcontroll length = " + videoListCollector.Length.ToString());
		//		Debug.Log ("index " + index.ToString ());
		//		Debug.Log ("video list length = " + videoListCollector [index].videoClip.Length.ToString());
		//		Debug.Log (nextVideo.ToString ());
		if (index >= videoListCollector.Length) {
			yield break;
		}

		if (nextVideo >= videoListCollector [index].videoClip.Length) {
			yield break;
		}

		Application.runInBackground = true;

		if (gameObject.GetComponent<VideoPlayer> () == null) {
			videoPlayer = gameObject.AddComponent<VideoPlayer> ();
			audioSource = gameObject.AddComponent<AudioSource> ();
		} else {
			videoPlayer = gameObject.GetComponent<VideoPlayer> ();
			audioSource = gameObject.GetComponent<AudioSource> ();
		}

		videoPlayer.playOnAwake = false;
		audioSource.playOnAwake = false;


		videoPlayer.source = VideoSource.VideoClip;
		videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;
		videoPlayer.renderMode = VideoRenderMode.RenderTexture;
		videoPlayer.EnableAudioTrack(0, true);
		videoPlayer.SetTargetAudioSource(0, audioSource);
		videoPlayer.clip = videoListCollector [index].videoClip [nextVideo];
		videoPlayer.Prepare();
		videoPlayer.isLooping = false;

		while (!videoPlayer.isPrepared)
			yield return null;

		videoPlayer.targetTexture = _renderTexture;
		audioSource.Play();
		videoPlayer.Play();

		if (nextVideo < videoListCollector [index].videoClip.Length) {
			videoPlayer.loopPointReached += EndReached;
		}

		while (videoPlayer.isPlaying)

			yield return null;

		//ScreenFader.FadeIn(2.5f, () => SceneManager.LoadScene("Menu"));
	}

	void EndReached(UnityEngine.Video.VideoPlayer vp)
	{
		nextVideo += 1;
		if (nextVideo >= videoListCollector [currentWindowIndex].videoClip.Length) {
			NextWindow ();
		} else {
			StartCoroutine (SetUpVideo(currentWindowIndex));
		}

	}

	public void RestartVideo()
	{
		if (txt != null) {
			txt.text = "work";
		}

		if (!videoListCollector [currentWindowIndex].isStaticSkybox) {
			videoPlayer.Stop ();
			audioSource.time = 0;
			audioSource.Play();
			videoPlayer.Play();
		} 
	}

	public void StopVideo()
	{
		if (!videoListCollector [currentWindowIndex].isStaticSkybox) {
			videoPlayer.Stop ();
			audioSource.time = 0;
		}
		//SceneManager.LoadScene("Main_Scene");
	}

	public void Looping(bool isLopping)
	{
		if (!videoListCollector [currentWindowIndex].isStaticSkybox) {
			videoPlayer.isLooping = isLopping;
		}
	}

}
