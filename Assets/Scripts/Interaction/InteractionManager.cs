﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractionManager : MonoBehaviour 
{
    public static InteractionManager instance = null;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if(instance !=this)
        {
            Destroy(gameObject);
        }
    }

    [SerializeField]
    private RectTransform rect;

    private bool withinSwipeUpRegion;
    private Vector2 touchPositionDown, touchPostionUp;

    [SerializeField]
    private float swipeUpThreshold;


	void SwipeUpCheck () 
    {
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);  

            if (touch.phase == TouchPhase.Began)
            {
                touchPositionDown = touch.position;

                withinSwipeUpRegion = RectTransformUtility.RectangleContainsScreenPoint(rect, touch.position);
            }

            if (touch.phase == TouchPhase.Ended && withinSwipeUpRegion)
            {
                touchPostionUp = touch.position;
                Debug.Log(CheckForSwipeUp());
            }
        }     
	}

    public bool CheckForSwipeUp()
    {
        if ((touchPostionUp.y - touchPositionDown.y) > swipeUpThreshold)
        {
            //Debug.Log("Touch delta" + (touchPostionUp.y - touchPositionDown.y));
            return true;
        }

        return false;
    }

}
