﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using TMPro;
using UnityEngine.SceneManagement;
using Malee;
using FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples;

public class VideoController : MonoBehaviour 
{
    public static VideoController instance;

    [SerializeField]
    private SpeechToTextManager speechToTextManager;

    [SerializeField]
    private GazeChecker gazeChecker;

	//This section is used to controll which videos play at which time. 
	//Make sure this list has the same number as the MenuWindowInfo
	[SerializeField, Reorderable(paginate = true, pageSize = 0)]
	public VideoListCollector videoListCollector;

	[System.Serializable]
	public struct VideoWindow 
	{
		[SerializeField]
		public string menuWindowName;

		[SerializeField]
		public Material skyboxMaterial;

		[SerializeField]
		public bool isStaticSkybox;

		[SerializeField, Reorderable(paginate = true, pageSize = 0)]
		public VideoList videoClip;

		[SerializeField, Reorderable(paginate = true, pageSize = 0)]
		public VideoOrganiser isVideoLooped;

	}

	[System.Serializable]
	public class VideoListCollector : ReorderableArray<VideoWindow> {}

	[System.Serializable]
	public class VideoList : ReorderableArray<VideoClip> {}

	[System.Serializable]
	public class VideoOrganiser : ReorderableArray<bool> {}

	[SerializeField]
	private GameObject[] controller;

	public int currentWindowIndex;

	[SerializeField]
	private RenderTexture _renderTexture = null;

	[SerializeField]
	private TextMeshProUGUI txt;

	[SerializeField]
	private MenuWindowInfo menuWindowInfo;

    [SerializeField]
    private LanguageScriptableObject[] languageSOList;

	[SerializeField]
	private bool isPublicSpeaking;

	private VideoPlayer videoPlayer;
	private AudioSource audioSource;
	private int nextVideo = 0;

    [Header("Audio/Animation/Script")]

    [Space]

    [SerializeField]
    private AudioAnimationScriptableObject[] aaSO;
   
    public GameObject becaGO;
    public Animator becaAnimator;
    public AudioSource becaAudioSource;
	public AudioSource pureAudioSource;
	public AudioClip ringtone;

    [SerializeField] 
    private TextMeshProUGUI[] subtitleTextBoxes;

	[Header("Interview Section")]

	[Space]
//
//	[SerializeField]
//	private AudioAnimationScriptableObject[] aaSOInterview;

	[SerializeField]
	private GameObject subBoxInterview;

	private int interviewIndex = 0;

    public Coroutine playSubtitlesCoroutine;
	private Coroutine timeDelayAudioCoroutine;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
        
	void Start()
	{
		controller [0].SetActive	(true);
		RenderSettings.skybox = videoListCollector [currentWindowIndex].skyboxMaterial;
		if (SubtitleManager.instance != null) {

			SubtitleManager.instance.StartSubtitles (0);
			Debug.Log ("Starting subtitles");
		} 
        AssignTextToWindowTextBoxes();

		if (isPublicSpeaking) {
			PublicSpeakingTrigger (currentWindowIndex);
		} else {
			InterviewSpeakingTrigger (currentWindowIndex);
		}
	}

	void Update()
	{
		if (Input.GetKeyDown("a")){
			NextWindow();
		}

        if (Input.GetKeyDown("w"))
        {
            speechToTextManager.StopRecordButtonOnClickHandler();
        }
	}

	//Play next in timeline
	public void NextWindow()
	{
		//Stop video play when moving to next screen 
		StopVideo ();

		menuWindowInfo.menuWindowList[currentWindowIndex].menuWindowGO.SetActive(false);

		//Move to next item
		currentWindowIndex++;

		if (SubtitleManager.instance != null) {

			SubtitleManager.instance.StartSubtitles (currentWindowIndex);
		} 

		//Reset next video list back to 0
		nextVideo = 0;

		//reset interview aaSo
		interviewIndex = 3;

		//Change the Skybox material from either video or static 
		RenderSettings.skybox = videoListCollector [currentWindowIndex].skyboxMaterial;

		//Start Video playing 
		if (!videoListCollector [currentWindowIndex].isStaticSkybox) {
			
			StartCoroutine (SetUpVideo(currentWindowIndex));
		}
            
		//Make next Window visible
		menuWindowInfo.menuWindowList[currentWindowIndex].menuWindowGO.SetActive(true);

        //Disable Finish button right when the public speaking video starts
        if (isPublicSpeaking && currentWindowIndex == 5)
        {
            ChangeController(1);
            controller[1].SetActive(false);

            gazeChecker.enabled = true;

            //Start Google Speech To Text when the Video Loop for the audience starts
            Debug.Log("Started Speech to Text");
            StartCoroutine(InitializeSpeechToText());
        }
   
        AssignTextToWindowTextBoxes();

		if (isPublicSpeaking) {
			
			PublicSpeakingTrigger (currentWindowIndex);
		} else {
			
			InterviewSpeakingTrigger (currentWindowIndex);
		}
	}

    private void PublicSpeakingTrigger(int currentWindowIndex)
    {
        switch (currentWindowIndex)
        {
            case 0:

                TriggerAudioAnimationScript(0,false);
                break;

            case 1:
			
                becaGO.SetActive(false);
                TriggerAudioAnimationScript(1,false);
                break;

            case 2:

                becaGO.SetActive(true);
                TriggerAudioAnimationScript(2,false);
                break;

            case 3:
                //Controlled by QA logic (Write triggers the next audio
                break;

            case 4:
                //Controlled by QA logic (Write triggers the next audio
                break;

            case 5:

                becaGO.SetActive(false);
                QAManager.instance.skipButton.interactable = false;

                break;
        }
    }


	private void InterviewSpeakingTrigger(int currentWindowIndex)
	{
		switch (currentWindowIndex)
		{
		case 0:

			TriggerAudioAnimationScript(0,false);
			break;

		case 1:
			
			becaGO.SetActive (false);
			timeDelayAudioCoroutine = StartCoroutine (TimeDelaySubtitles ());
			break;

		case 2:
			
			if (timeDelayAudioCoroutine != null) {
				StopCoroutine (timeDelayAudioCoroutine);
			}

			becaGO.SetActive (true);
			timeDelayAudioCoroutine = null;
			TriggerAudioAnimationScript(2, false);
			break;

		case 3:
			//Controlled by QA logic (Write triggers the next audio
			break;

		case 4:
			//Controlled by QA logic (Write triggers the next audio
			break;

		case 5:

			becaGO.SetActive (false);
			TriggerAudioAnimationScript(interviewIndex,false);
			break;
		}
	}

	IEnumerator TimeDelaySubtitles()
	{
		//Interview host window Ringtone sound effect
		pureAudioSource.clip = ringtone;
		pureAudioSource.Play ();
		if (playSubtitlesCoroutine != null)
		{
			StopCoroutine(playSubtitlesCoroutine);
		}

		yield return new WaitForSeconds (10);
		TriggerAudioAnimationScript(1,true);
	}

	public void TriggerAudioAnimationScript(int index, bool isPureAudo)
    {
//		Debug.Log ("INDEX OF AASO = " + index);
        if (aaSO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].audioClip != null)
        {
	
	
			if (becaAudioSource.isPlaying) 
			{
				becaAudioSource.Stop ();
			}


			//Only for Interview Host_intro_Window
			if (pureAudioSource != null && pureAudioSource.isPlaying) 
			{
				pureAudioSource.Stop ();
			}

	

			if (!isPureAudo) 
			{
				becaAudioSource.clip = aaSO [(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList [index].audioClip;
				becaAudioSource.Play ();
			} else {
				//Only for Interview Host_intro_Window
				pureAudioSource.clip = aaSO [(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList [index].audioClip;
				pureAudioSource.Play ();
			}

            if (aaSO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].animationName == "Talking")
            {
                becaAnimator.SetBool("isTalking", true);
            }
		}

        if (playSubtitlesCoroutine != null)
        {
            StopCoroutine(playSubtitlesCoroutine);
        }

        playSubtitlesCoroutine = StartCoroutine(PlaySubtitles(index));
    }

    public void StopPlaySubtitlesCoroutine()
    {
        StopCoroutine(playSubtitlesCoroutine);
    }

    IEnumerator PlaySubtitles(int index)
    {
        //Finding Total time to run the coroutine for
        float totalSubtitleTime = 0;

        for (int i = 0; i < aaSO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].subtitleInfoList.Length; i++)
        {
            totalSubtitleTime += aaSO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].subtitleInfoList[i].subtitleDisplayTime;
        }

        float startTime = Time.time;
        float deltaTime = 0;

        float displayTime = aaSO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].subtitleInfoList[0].subtitleDisplayTime;
        subtitleTextBoxes[aaSO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].subtitleTextboxIndex].text 
            = aaSO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].subtitleInfoList[0].subtiteText;

        int j = 0;

        //subtracting 1 to prevent the coroutine from jumping just over the totalSubtitleTime
        while (deltaTime < (totalSubtitleTime-1))
        {
            deltaTime = Time.time - startTime;

            if (deltaTime > displayTime)
            {
                j++;
                displayTime+= aaSO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].subtitleInfoList[j].subtitleDisplayTime;
                subtitleTextBoxes[aaSO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].subtitleTextboxIndex].text
                    = aaSO[(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList[index].subtitleInfoList[j].subtiteText;
            }

            yield return null;
        }

//		//Disable Finish button right when the public speaking video starts
//		if (!isPublicSpeaking && currentWindowIndex == 5)
//		{
//			ChangeController(2);
//			//controller[1].SetActive(false);
//
//			gazeChecker.enabled = true;
//
//			//Start Google Speech To Text when the Video Loop for the audience starts
//			Debug.Log("Started Speech to Text");
//			StartCoroutine(InitializeSpeechToText());
//		}
        becaAnimator.SetBool("isTalking", false);
    }

    IEnumerator InitializeSpeechToText()
    {
        yield return new WaitForSeconds(4f);
        speechToTextManager.StartRecordButtonOnClickHandler();

        yield return new WaitForSeconds(5f);
        controller[1].SetActive(true);
    }

    public void Stop()
    {
        speechToTextManager.StopRecordButtonOnClickHandler();
        //ChangeController(0);
    }

    public void AssignTextToWindowTextBoxes()
    {
        //int languageIndex = (int) SceneInfoManager.instance.selectedLanguage;
        int languageIndex = 0;

        //Debug.Log(languageIndex);
        for (int i = 0; i < menuWindowInfo.menuWindowList[currentWindowIndex].textGOList.Count; i++)
        {

            menuWindowInfo.menuWindowList[currentWindowIndex].textGOList[i].GetComponent<TextMeshProUGUI>().text = 
                languageSOList[languageIndex].menuWindowList[currentWindowIndex].textInfoList[i].textInfo;

            menuWindowInfo.menuWindowList[currentWindowIndex].textGOList[i].GetComponent<TextMeshProUGUI>().overrideColorTags = true;

            menuWindowInfo.menuWindowList[currentWindowIndex].textGOList[i].GetComponent<TextMeshProUGUI>().parseCtrlCharacters = true;
        }
    }

    private IEnumerator SetUpVideo(int index)
	{
		Debug.Log (videoListCollector [index].videoClip [nextVideo].name);
		if (index >= videoListCollector.Length) {
			yield break;
		}

		if (nextVideo >= videoListCollector [index].videoClip.Length) {
			yield break;
		}

		//Application.runInBackground = true;

		if (gameObject.GetComponent<VideoPlayer> () == null) {
			videoPlayer = gameObject.AddComponent<VideoPlayer> ();
			audioSource = gameObject.AddComponent<AudioSource> ();
		} else {
			videoPlayer = gameObject.GetComponent<VideoPlayer> ();
			audioSource = gameObject.GetComponent<AudioSource> ();
		}

		videoPlayer.playOnAwake = false;
		audioSource.playOnAwake = false;

		videoPlayer.source = VideoSource.VideoClip;
		videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;
		videoPlayer.renderMode = VideoRenderMode.RenderTexture;
		videoPlayer.EnableAudioTrack(0, true);
		videoPlayer.SetTargetAudioSource(0, audioSource);
		videoPlayer.clip = videoListCollector [index].videoClip [nextVideo];
		videoPlayer.Prepare();

//		//Checks to see is video need to be looped.
		if (videoListCollector [index].isVideoLooped != null) {
			videoPlayer.isLooping = videoListCollector [index].isVideoLooped [nextVideo];

			//Check which section of the app they are in.
			//Checks if they are in the main event of the app. 
			if (!isPublicSpeaking && videoListCollector [index].videoClip.Length > 1) {


				//checks to see if video is looping video order is question ... hold ... question ... hold. 
				if (videoPlayer.isLooping) 
				{
//					Debug.Log ("LOOPING");
					//Show Controll Panel 
					ChangeController (2);

					if (subBoxInterview != null) 
					{
						if (!isPublicSpeaking) {
							interviewIndex +=1;
						}
						subBoxInterview.SetActive (false);
					}

				} else {
					
					//Hide Controll Panel 
//					Debug.Log ("NOT LOOPING");
					ChangeController (5);

					if (subBoxInterview != null) 
					{
						subBoxInterview.SetActive (true);
					}

				}
			}
		}

		while (!videoPlayer.isPrepared)
			yield return null;

		videoPlayer.targetTexture = _renderTexture;
		audioSource.Play();
		videoPlayer.Play();


		if (nextVideo < videoListCollector [index].videoClip.Length)
        {
			videoPlayer.loopPointReached += EndReached;
		}

		while (videoPlayer.isPlaying)
			
			yield return null;

		//ScreenFader.FadeIn(2.5f, () => SceneManager.LoadScene("Menu"));
	}

	void PlayAgain(UnityEngine.Video.VideoPlayer vp)
    {
		vp.isLooping = true;
//		RestartVideo ();
	}

	void EndReached(UnityEngine.Video.VideoPlayer vp)
	{
		if (videoListCollector [currentWindowIndex].isVideoLooped [nextVideo] == true) 
        {
			videoPlayer.isLooping = true;

		}
        else
        {	
			NextVideo ();
		}			
	}

	public void NextVideo()
    {
        nextVideo += 1;

		Debug.Log ("INDEX OF NEXT VIDEO = " + nextVideo);
		Debug.Log (" INTERVIEW INDEX = " + interviewIndex);
		if (nextVideo >= videoListCollector [currentWindowIndex].videoClip.Length) 
        {
			Debug.Log ("Next window");
			NextWindow ();

		} else 
        { 
			
			if (!isPublicSpeaking) 
			{
				if (interviewIndex < aaSO [(int)SceneInfoManager.instance.selectedLanguage].audioAnimationInfoList.Length) {
				
					InterviewSpeakingTrigger (currentWindowIndex);
				}
			}
//			Debug.Log ("Next Video");
			StartCoroutine (SetUpVideo(currentWindowIndex));
		}
	}

//	public void NextInterviewVideo()
//	{
//		
//		interviewIndex = +1;
//		nextVideo += 1;
//		StartCoroutine (SetUpVideo(currentWindowIndex));
//
//
//	}

	public void RestartVideo()
	{
        /*
		if (txt != null) {
			txt.text = "work";
		}

		if (!videoListCollector [currentWindowIndex].isStaticSkybox) {
			videoPlayer.Stop ();
			audioSource.time = 0;
			audioSource.Play();
			videoPlayer.Play();
		} 
     */
        SceneManager.LoadScene("Public_Scene");
	}

	public void StopVideo()
	{
		if (!videoListCollector [currentWindowIndex].isStaticSkybox) {
			videoPlayer.Stop ();
			audioSource.time = 0;
		}

	}

	public void Looping(bool isLopping)
	{
		if (!videoListCollector [currentWindowIndex].isStaticSkybox) {
			videoPlayer.isLooping = isLopping;
		}
	}

	public void ChangeController(int pos){
		
		for (int i = 0; i < controller.Length; i++) {
			if (i == pos ) {
				
				controller [i].SetActive(true);
			} else {
				controller [i].SetActive(false);
			}
		}
	}

	public void Exit()
	{
		StopVideo ();
		SceneManager.LoadScene("Main_Scene");
	}
}
