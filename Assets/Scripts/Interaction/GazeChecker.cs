﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GazeChecker : MonoBehaviour
{
    public float gazeAtAudienceTime, gazeCheckTotalTime;
    public bool finishedSpeech;

    void Start()
    {
        Invoke("StartGazeCheck", 5f);
    }

    void StartGazeCheck()
    {
        //Debug.Log("Starting Check for gaze");
        StartCoroutine(GazeCheck());
    }

    IEnumerator GazeCheck()
    {
        float gazeCheckStartTime, gazeCheckEndTime;

        gazeCheckStartTime = Time.time;
       
        while (finishedSpeech)
        {
            Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            RaycastHit rayCastHit;

            Debug.DrawRay(ray.origin, ray.direction * 30f, Color.green);
            if (Physics.Raycast(ray, out rayCastHit, 30f))
            {
                if (rayCastHit.collider != null && rayCastHit.collider.gameObject.layer == 9)
                {
                    Debug.Log("Looking At audience");
                    gazeAtAudienceTime += Time.deltaTime;
                }
            }

       

            yield return null;
        }

        gazeCheckEndTime = Time.time;
        gazeCheckTotalTime = gazeCheckEndTime - gazeCheckStartTime;

        /*
         
        Debug.Log("Gaze Start Time: " + gazeCheckStartTime);
        Debug.Log("Gaze End Time: " + gazeCheckEndTime);
        //Debug.Log("Total Time with no eye contact: " + (totalGazeCheckTime - gazeAtAudienceTime));

        Debug.Log("Total Gaze Time" + gazeCheckTotalTime);

        */

        TextAnalysisManager.instance.GazeCheck();
    }

}
