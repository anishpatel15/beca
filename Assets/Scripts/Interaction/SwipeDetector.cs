﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwipeDetector : MonoBehaviour
{
	private Vector2 fingerDown;
	private Vector2 fingerUp;
	public bool detectSwipeOnlyAfterRelease = true;

	public Text txt;
	private GameObject arrow;

	[SerializeField]
	private GameObject homeScreenMenu;
	[SerializeField]
	private GameObject mainMenu;


	public float SWIPE_THRESHOLD = 80f;

	[SerializeField]
	public float targetHeightInPercent = 20f;

	bool isUp = false;
	//private float maxThreshhold;
	private float menuHeight;
	private float downTime; 
	private float startTime;
	private float curTime;

	void Start()
	{

		int height = Screen.height; 

//		maxThreshhold = (height / 100) * targetHeightInPercent;

		RectTransform menuTransform = (RectTransform)mainMenu.transform;
		menuHeight = menuTransform.rect.height;

		//set inital pos of the bottom menu
		Vector2 temp = mainMenu.transform.position;
		temp.y = - menuHeight;
		mainMenu.transform.position = temp; 


	}

	// Update is called once per frame
	void Update()
	{


		foreach (Touch touch in Input.touches)
		{
			if (touch.phase == TouchPhase.Began)
			{
				
				fingerUp = touch.position;
				fingerDown = touch.position;
				startTime = Time.time;

			}

			//Detects Swipe while finger is still moving
			if (touch.phase == TouchPhase.Moved)
			{
				
//				Vector2 temp = transform.position;
//				temp.y = Mathf.Clamp(touch.position.y, 0, maxThreshhold);
//				transform.position = temp; 
//
//				 
////				fadeWithDistance (temp);
//				if (curTime > 0.340f) 
//				{
////					Vector2 temp = homeScreenMenu.transform.position;
////					temp.y = Mathf.Clamp(touch.position.y, 0, maxThreshhold);
////					homeScreenMenu.transform.position = temp; 
//					float parameter = Mathf.InverseLerp (0, 400, touch.position.y);
//					txt.text = parameter.ToString ("0000.00");
//				}


				//txt.text = curTime.ToString ("0.000");
				//txt.text = Mathf.Clamp(touch.position.y, 0, maxThreshhold).ToString();//touch.position.y.ToString ();

				if (detectSwipeOnlyAfterRelease)
				{
					fingerDown = touch.position;
					checkSwipe();
				}
				//curTime = Time.time - startTime;
			}

			//Detects swipe after finger is released
			if (touch.phase == TouchPhase.Ended)
			{
				
				fingerDown = touch.position;
				//txt.text = curTime.ToString ("0.000");
				//downTime = 0f;
				checkSwipe();
				curTime = Time.time - startTime;

			}
		}
	}

	void checkSwipe()
	{
		//Check if Vertical swipe
		if (verticalMove() > SWIPE_THRESHOLD && verticalMove() > horizontalValMove())
		{
			//Debug.Log("Vertical");
			if (fingerDown.y - fingerUp.y > 0)//up swipe
			{
				OnSwipeUp();
			}
			else if (fingerDown.y - fingerUp.y < 0)//Down swipe
			{
				OnSwipeDown();
			}
			fingerUp = fingerDown;
		}

		//Check if Horizontal swipe
		else if (horizontalValMove() > SWIPE_THRESHOLD && horizontalValMove() > verticalMove())
		{
			//Debug.Log("Horizontal");
			if (fingerDown.x - fingerUp.x > 0)//Right swipe
			{
				OnSwipeRight();
			}
			else if (fingerDown.x - fingerUp.x < 0)//Left swipe
			{
				OnSwipeLeft();
			}
			fingerUp = fingerDown;
		}

		//No Movement at-all
		else
		{
			//Debug.Log("No Swipe!");
		}
	}
		

	float verticalMove()
	{
		return Mathf.Abs(fingerDown.y - fingerUp.y);
	}

	float horizontalValMove()
	{
		return Mathf.Abs(fingerDown.x - fingerUp.x);
	}

	//////////////////////////////////CALLBACK FUNCTIONS/////////////////////////////
	void OnSwipeUp()
	{

		if (!isUp) {
			if (curTime <= 0.530f) {
				StartCoroutine (fadeWithTime (false, homeScreenMenu));
				iTween.MoveTo (homeScreenMenu, iTween.Hash ("y", menuHeight, "islocal", false, "easetype", "easeInOutQuad", "time", 0.2f));
				iTween.MoveTo (mainMenu, iTween.Hash ("y", 0, "islocal", false, "easetype", "easeInOutQuad", "time", 0.2f));
				StartCoroutine (fadeWithTime (true, mainMenu));
				isUp = true;
			}
		}
		//txt.text = "Swipe UP";
	}

	void OnSwipeDown()
	{
		if (isUp) {
			if (curTime <= 0.530f) {
				StartCoroutine (fadeWithTime (true, homeScreenMenu));
				iTween.MoveTo (homeScreenMenu, iTween.Hash ("y", 0, "islocal", false, "easetype", "easeInOutQuad", "time", 0.2f));
				iTween.MoveTo (mainMenu, iTween.Hash ("y", -menuHeight, "islocal", false, "easetype", "easeInOutQuad", "time", 0.2f));
				StartCoroutine (fadeWithTime (false, mainMenu));
				isUp = false;
			}
		}
//		txt.text = "Swipe Down";
	}

	void OnSwipeLeft()
	{
//		txt.text = "Swipe Left";
	}

	void OnSwipeRight()
	{
//		txt.text = "Swipe Right";
	}



	void fadeWithDistance(Vector2 temp){

		Image[] tempImage = homeScreenMenu.GetComponentsInChildren<Image> ();
		int count = 0;

		foreach (Image img in tempImage) 
		{

			//Confine y Co-ord to alpha values
			float parameter = Mathf.InverseLerp (255, 0, temp.y);

			Color tempColour = img.color;
			tempColour.a = parameter;
			homeScreenMenu.GetComponentsInChildren<Image> () [count].color = tempColour;

			count += 1;

		}

	}
		

	IEnumerator fadeWithTime (bool fadeIn, GameObject gameObject)
	{
		
		float lerpIn = 0F;
		float lerpOut = 1F;
		float timeToStart = Time.time;
		float speed = 3.2f; 

		if (fadeIn) {
			while (lerpIn < 1.0f) 
			{
				lerpIn = Mathf.Lerp (0f, 1f, (Time.time - timeToStart) * speed);
				gameObject.GetComponent<CanvasGroup> ().alpha = lerpIn;

				yield return null;
			}
		} else 
		{
			while (lerpOut > 0.0f) 
			{
				lerpOut = Mathf.Lerp (1f, 0f, (Time.time - timeToStart) * speed);
				gameObject.GetComponent<CanvasGroup> ().alpha = lerpOut;

				yield return null;
			}
		}

	}


	public void Open(){
		
		if (isUp) 
		{
			OnSwipeDown ();
		} else {
			OnSwipeUp ();
		}
	}

}