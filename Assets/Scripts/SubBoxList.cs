﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SubBoxList : MonoBehaviour {

	public static SubBoxList instance = null;

	void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else if(instance !=this)
		{
			Destroy(gameObject);
		}
	}

	[SerializeField]
	private TextMeshProUGUI[] textBox;

	public TextMeshProUGUI GetSubTextBox(int pos){

		if (pos < textBox.Length) {
			return textBox [pos];
		}
		return null;
		
	}
}
