﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockOrientation : MonoBehaviour {

	[SerializeField]
	private bool isPortrait = true;

	// Use this for initialization
	void Start () {
		if(isPortrait){
			Screen.orientation = ScreenOrientation.Portrait;
		}else{
			Screen.orientation = ScreenOrientation.Landscape;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
