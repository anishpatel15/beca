﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class KeyCodeBehaviour : MonoBehaviour {
	
	public float moveSpeed = 10f;
	public float turnSpeed = 50f;

	Vector3 FirstPoint;
	Vector3 SecondPoint;
	float xAngle;
	float yAngle;
	float xAngleTemp;
	float yAngleTemp;

	void Start () {
		#if UNITY_ANDROID
			xAngle = 0;
			yAngle = 0;
			this.transform.rotation = Quaternion.Euler(yAngle, xAngle, 0);    
		#endif

		#if UNITY_EDITOR
			Random.seed = (int)Time.time;
		#endif
	}




	void Update () {
		#if UNITY_ANDROID
		if(Input.touchCount > 0){
			if(Input.GetTouch(0).phase == TouchPhase.Began){
				FirstPoint = Input.GetTouch(0).position;
				xAngleTemp = xAngle;
				yAngleTemp = yAngle;
			}
			if(Input.GetTouch(0).phase == TouchPhase.Moved){
				SecondPoint = Input.GetTouch(0).position;
				xAngle = xAngleTemp + (SecondPoint.x - FirstPoint.x) * 180 / Screen.width;
				yAngle = yAngleTemp + (SecondPoint.y - FirstPoint.y) * 90 / Screen.height;
				this.transform.rotation = Quaternion.Euler(yAngle, xAngle, 0.0f);
			}
		}

		#endif

		#if UNITY_EDITOR 
		if(Input.GetKey(KeyCode.UpArrow))
			transform.Rotate(Vector3.left, turnSpeed * Time.deltaTime);

		if(Input.GetKey(KeyCode.DownArrow))
			transform.Rotate(Vector3.right, turnSpeed * Time.deltaTime);

		if(Input.GetKey(KeyCode.LeftArrow))
			transform.Rotate(Vector3.up, -turnSpeed * Time.deltaTime);

		if(Input.GetKey(KeyCode.RightArrow))
			transform.Rotate(Vector3.up, turnSpeed * Time.deltaTime);
		#endif
	}
}