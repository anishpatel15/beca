﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MecanimController : MonoBehaviour {

    public AudioClips audioScript;
    public AudioSource Audio;
    public Animator anim;

    // Use this for initialization
    void Start() {

        anim = GetComponent<Animator>();
        audioScript = Audio.GetComponent<AudioClips>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        //Talking();
        Idle();
        SoundStart();
        //Asking();
        //PointingUp();
    }

    public void SoundStart()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine(gameObject.GetComponent<AudioClips>().playSound());
        }
    }

    //public void Talking()
    //{

    //    if (Audio.GetComponent<AudioSource>().isPlaying && Audio.clip != Audio.GetComponent<AudioClips>().Clip_007)
    //    {
    //        Debug.Log("talking");
    //        anim.SetBool("isTalking", true);
    //    } 
    //    else
    //    {
    //        anim.SetBool("isTalking", false);
    //    }
    //}

    public void Idle()
    {
        anim.SetBool("isIdle", true);

        //if (!Audio.GetComponent<AudioSource>().isPlaying || Input.GetKeyDown("i"))
        //{
        //    Debug.Log("silent");
        //    anim.SetBool("isIdle", true);
        //}
        //else
        //{
        //    anim.SetBool("isIdle", false);
        //}
    }

    public void TalkStart()
    {
        anim.SetBool("isTalking", true);
    }

    public void TalkStop()
    {
        anim.SetBool("isTalking", false);
    }

    public void AskStart()
    {
        anim.SetBool("isAsking", true);
    }

    public void AskStop()
    {
        anim.SetBool("isAsking", false);
    }

    public void PoseStart()
    {
        anim.SetBool("isPosing", true);
    }

    public void PoseStop()
    {
        anim.SetBool("isPosing", false);
    }

    public void PointStart()
    {
        anim.SetBool("isPointing", true);
    }

    public void PointStop()
    {
        anim.SetBool("isPointing", false);
    }

    public void GoodAnswerStart()
    {
        anim.SetBool("isGood", true);
    }

    public void GoodAnswerStop()
    {
        anim.SetBool("isGood", false);
    }

    public void WrongAnswerStart()
    {
        anim.SetBool("isWrong", true);
    }

    public void WrongAnswerStop()
    {
        anim.SetBool("isWrong", false);
    }

}
