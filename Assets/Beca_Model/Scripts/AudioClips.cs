﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioClips : MonoBehaviour {

    public AudioClip Clip_001;
    public AudioClip Clip_002;
    public AudioClip Clip_003;
    public AudioClip Clip_004;
    public AudioClip Clip_005;
    public AudioClip Clip_006;
    public AudioClip Clip_007;
    public AudioClip Clip_008;
    public AudioClip Clip_009;
    public AudioClip Clip_010;
    public AudioClip Clip_011;
    public AudioClip Clip_012;
    public AudioClip Clip_013;
    public AudioClip Clip_014;
    public AudioClip Clip_015;
    public AudioClip Clip_016;
    public AudioClip Clip_017;
    public AudioClip Clip_018;
    public AudioClip Clip_019;
    public AudioClip Clip_020;
    public AudioClip Clip_021;
    public AudioClip Clip_022;
    public AudioClip Clip_023;
    public AudioClip Clip_024;
    public AudioClip Clip_025;
    public AudioClip Clip_026;
    public AudioClip Clip_027;
    public AudioClip Clip_028;
    public AudioClip Clip_029;
    public AudioClip Clip_030;
    public AudioClip Clip_031;
    public AudioClip Clip_032;
    public AudioClip Clip_033;
    public AudioClip Clip_034;
    public AudioClip Clip_035;
    public AudioClip Clip_036;
    public AudioClip Clip_037;
    public AudioClip Clip_038;
    public AudioClip Clip_039;
    public AudioClip Clip_040;
    public AudioClip Clip_041;
    public AudioClip Clip_042;
    public AudioClip Clip_043;
    public AudioClip Clip_044;
    public AudioClip Clip_045;
    public AudioClip Clip_046;
    public AudioClip Clip_047;
    public AudioClip Clip_048;
    public AudioClip Clip_049;
    public AudioClip Clip_050;
    public AudioClip Clip_051;
    public AudioClip Clip_052;
    public AudioClip Clip_053;
    public AudioClip Clip_054;
    public AudioClip Clip_055;
    public AudioClip Clip_056;
    public AudioClip Clip_057;
    public AudioClip Clip_058;
    public AudioClip Clip_059;
    public AudioClip Clip_060;
    public AudioClip Clip_061;
    public AudioClip Clip_062;
    public AudioClip Clip_063;
    public AudioClip Clip_064;
    public AudioClip Clip_065;
    public AudioClip Clip_066;
    public AudioClip Clip_067;
    public AudioClip Clip_068;
    public AudioClip Clip_069;
    public AudioClip Clip_070;
    public AudioClip Clip_071;
    public AudioClip Clip_072;


    public AudioSource audioSource;
   
    public MecanimController Mecanim;

    // Use this for initialization
    void Start()
    {
        //StartCoroutine(playSound());
    }
                
    public IEnumerator playSound()
    {

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_001;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_001.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_002;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_002.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_003;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_003.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_004;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_004.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_005;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_005.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_006;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_006.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.AskStart();
        GetComponent<AudioSource>().clip = Clip_007;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_007.length);
        Mecanim.AskStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_008;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_008.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.AskStart();
        GetComponent<AudioSource>().clip = Clip_009;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_009.length);
        Mecanim.AskStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_010;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_010.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_011;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_011.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.AskStart();
        GetComponent<AudioSource>().clip = Clip_012;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_012.length);
        Mecanim.AskStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_013;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_013.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_014;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_014.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.AskStart();
        GetComponent<AudioSource>().clip = Clip_015;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_015.length);
        Mecanim.AskStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_016;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_016.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_017;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_017.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.AskStart();
        GetComponent<AudioSource>().clip = Clip_018;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_018.length);
        Mecanim.AskStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_019;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_019.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_020;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_020.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_021;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_021.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.AskStart();
        GetComponent<AudioSource>().clip = Clip_022;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_022.length);
        Mecanim.AskStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_023;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_023.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_024;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_024.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_025;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_025.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_026;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_026.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_027;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_027.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_028;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_028.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_029;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_029.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_030;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_030.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_031;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_031.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_032;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_032.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.PointStart();
        GetComponent<AudioSource>().clip = Clip_033;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_034.length);
        Mecanim.PointStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.PoseStart();
        GetComponent<AudioSource>().clip = Clip_069;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_069.length);
        Mecanim.PoseStop();

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_070;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_070.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_057;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_057.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_058;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_058.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.GoodAnswerStart();
        GetComponent<AudioSource>().clip = Clip_035;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_035.length);
        Mecanim.GoodAnswerStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_036;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_036.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_059;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_059.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_071;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_071.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.PointStart();
        GetComponent<AudioSource>().clip = Clip_071;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_071.length);
        Mecanim.PointStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_037;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_037.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_038;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_038.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.GoodAnswerStart();
        GetComponent<AudioSource>().clip = Clip_039;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_039.length);
        Mecanim.GoodAnswerStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_040;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_040.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_041;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_041.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_042;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_042.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_061;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_061.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.GoodAnswerStart();
        GetComponent<AudioSource>().clip = Clip_062;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_062.length);
        Mecanim.GoodAnswerStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.WrongAnswerStart();
        GetComponent<AudioSource>().clip = Clip_046;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_046.length);
        Mecanim.WrongAnswerStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_047;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_047.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_048;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_048.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_049;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_049.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.WrongAnswerStart();
        GetComponent<AudioSource>().clip = Clip_050;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_050.length);
        Mecanim.WrongAnswerStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_061;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_061.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_062;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_062.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_063;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_063.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_064;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_064.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_065;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_065.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_066;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_066.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_067;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_067.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.TalkStart();
        GetComponent<AudioSource>().clip = Clip_054;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_054.length);
        Mecanim.TalkStop();

        // IDLE
        yield return new WaitForSeconds(2);

        Mecanim.PointStart();
        GetComponent<AudioSource>().clip = Clip_054;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(Clip_054.length);
        Mecanim.PointStop();

        // IDLE
        yield return new WaitForSeconds(2);


    }
}
